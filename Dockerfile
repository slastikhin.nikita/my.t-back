FROM python:3.6-alpine

RUN adduser -D microserv

WORKDIR /home/microserv

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY . ./
#/home/microserv
#COPY migrations migrations
#COPY microblog.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP run.py

RUN chown -R microserv:microserv ./
USER microserv

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
