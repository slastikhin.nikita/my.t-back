from views import app

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy(app)

class MainTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user_table.id'), nullable=False)
#    user_id = db.Column(db.String(100))
    machine_id = db.Column(db.Integer, db.ForeignKey('machine_table.id'), nullable=False)
#    machine_id = db.Column(db.String(100))
    date_time = db.Column(db.DateTime)
    status = db.Column(db.Boolean)


class UserHoursTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user_table.id'), nullable=False)
#    user_id = db.Column(db.String(100))
    machine_id = db.Column(db.Integer, db.ForeignKey('machine_table.id'), nullable=False)
#    machine_id = db.Column(db.String(100))
    month = db.Column(db.DateTime)
    hours = db.Column(db.DateTime)

class UserTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_name = db.Column(db.String(100),unique=True)


class MachineTable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    machine_name = db.Column(db.String(200),unique=True)


db.create_all()
