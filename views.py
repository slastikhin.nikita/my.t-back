from flask import Flask
from flask import render_template, jsonify, request
from flask_cors import CORS, cross_origin
import os
from flask_sqlalchemy import SQLAlchemy

import yaml
from datetime import datetime, date, time



app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


with open(os.path.join(app.root_path, 'config.yml')) as f:
    obj_conf = yaml.load(f, Loader=yaml.FullLoader)
app.config.from_mapping(obj_conf.get('FLASK_CONFIG'))




from model import db, MachineTable, UserTable, MainTable, UserHoursTable



@app.route('/')
@cross_origin()
def main():
    total_users = UserTable.query.count()
    total_mach = MachineTable.query.count()
    date = datetime.today()

#    return render_template('main.html', total_users=total_users,total_mach=total_mach,date=date.strftime("%d/%m/%Y"))
    return 'ok 13'

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.htm')


@app.route('/api/1.0/get_users')
@cross_origin()
def get_users():
    records = []
    recs = UserTable.query.all()

    for rec in recs:
        records.append({'id': rec.id,'name':rec.user_name})

    return jsonify(records)


@app.route('/api/1.0/get_machines')
@cross_origin()
def get_machines():
    records = []
    recs = MachineTable.query.all()

    for rec in recs:
        records.append({'id':rec.id,'name':rec.machine_name})

    return jsonify(records)


@app.route('/api/1.0/get_log')
@cross_origin()
def get_log():
    result = []
    mach_dic = {}
    users_dic = {}

    mach_id = request.args.get('mach_id')
    total_mach = MachineTable.query.all()
    total_users = UserTable.query.all()

    for mach in total_mach:
        mach_dic[mach.id] = mach.machine_name

    for usr in total_users:
        users_dic[usr.id] = usr.user_name


    if mach_id is not None:
        res_log = MainTable.query.filter_by(machine_id=int(mach_id)).all()
    else:
        res_log = ''

    for r in res_log:
        result.append({'usr':users_dic[r.user_id], 'dt':r.date_time, 'status':r.status})
    return jsonify(result)



@app.route('/api/1.0/get_years')
@cross_origin()
def get_years():
    result = []

    first_year = MainTable.query.order_by(MainTable.date_time.asc()).first()
    last_year = MainTable.query.order_by(MainTable.date_time.desc()).first()

    result = list(range(int(first_year.date_time.year), int(last_year.date_time.year)+1))

    return jsonify(result)

@app.route('/api/1.0/get_user_hours')
@cross_origin()
def get_user_hours():
    result = []
#    user_id, mach_id, year, month
    user_id = request.args.get('user_id')
#    mach_id = request.args.get('mach_id')
    year = request.args.get('year')
    month = request.args.get('month')
    search_date = datetime(int(year), int(month), 1, 0, 0, 0)
    if (user_id is None) or (year is None) or (month is None):
        result = ''
    else:
        rows = UserHoursTable.query.filter_by(user_id=int(user_id),month=search_date).all()
        for r in rows:
            a = r.hours - datetime(1970,1,1,0,0,0)
            totsec = a.total_seconds()
            h = totsec/3600
            result.append({'user_id':r.user_id, 'mach_id':r.machine_id, 'hours':str("{0:.3f}".format(round(h,3))), 'month':r.month})


    return jsonify(result)
