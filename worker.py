# 1. Read YML.config
#   - add new record to MachineTable (machine_id, name)
# 2. Read Log's
#   - parse string, 
#   - add new user in UserTable
#   - add record (user_id, machine_id, datetime, status) in MainTable
import yaml
from views import db
from model import MachineTable, UserTable, MainTable, UserHoursTable

from datetime import datetime, date, MINYEAR, MAXYEAR, timedelta
import calendar

def add_machine(machines):
    """
    Add new machine in db if not exist
    """
    for mach in machines:   
        record = MachineTable.query.filter_by(machine_name=mach).first()
        if record is None:
            row = MachineTable(machine_name=mach)
            db.session.add(row)
        db.session.commit()
    return True


def add_user(user_name):
    """
    TODO
    """
    if UserTable.query.filter_by(user_name=user_name).first() is None:
        row = UserTable(user_name=user_name)
        db.session.add(row)
        db.session.commit()
    return True

def add_log(name,path):
    """
    Adding logfile in db
    """
    mach_name = name
    with open(path) as f:
        for line in f.readlines():
            line_words = line.split()
            if len(line_words) > 4:     # Check for "two words name"
                name = line_words[0]+' '+line_words[1]
                dt = line_words[2] + ' ' + line_words[3]
                status = int(line_words[4])
            else:
                name = line_words[0]
                dt = line_words[1] + ' ' + line_words[2]
                status = int(line_words[3])
            add_user(name)


            name = UserTable.query.filter_by(user_name=name).first()
            mach = MachineTable.query.filter_by(machine_name=mach_name).first()
            date_time = datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')


            row = MainTable(user_id=name.id,machine_id=mach.id,date_time=date_time,status=bool(status))
            db.session.add(row)
            db.session.commit()
    # nikopol              2017-08-20 10:15:10 0






def parse_log(input_log, date_start=None, date_end=None, verbose=False):
    """
    Input date example: 2016-09-18

    Usage example:
    >>> parse_log("mylog", "2016-10-10", "2016-12-10")
    """

    users_dic = {}
    total_users = UserTable.query.all()
    for usr in total_users:
        users_dic[usr.id] = usr.user_name


    summary = dict()
    # Filling date boundaries
    if date_start:
        date_start = datetime.strptime(date_start, "%Y-%m-%d")
    else:
        date_start = datetime(MINYEAR, 1, 1, 0, 0, 0)

    if date_end:
        date_end = datetime.strptime(date_end, "%Y-%m-%d")
    else:
        date_end = datetime(MAXYEAR, 1, 1, 0, 0, 0)

    #Parse log file
    start_time = None
    start_owner = None
    start_stop_mode = None

    for line in input_log:
        line_words = line

        try:
            #datetime_string = line_words.date_time
            #date_object = datetime.strptime(datetime_string, "%Y-%m-%d %H:%M:%S")
            date_object = line_words.date_time
                #print line_words[3], start_stop_mode
            if line_words.status == True:
                start_time = date_object
                start_owner = users_dic[line_words.user_id]
            elif (line_words.status == False) and (start_stop_mode == True):
                if date_start < start_time < date_end:
                        # Flush data when '1' switches to '0'
                    consumed_time = (date_object-start_time)
                    try:
                            #Try to add if key exists
                        summary[start_owner] += consumed_time
                    except KeyError:
                            #Create new key if not exists
                        summary[start_owner] = consumed_time
                    if verbose != False:
                    #  print datetime_string, "Name:", start_owner, "Consumed time: ", consumed_time
                        print('{} Name: {} Consumed time: {}'.format(datetime_string, start_owner, consumed_time))
            start_stop_mode = line_words.status

        except ValueError:
            pass
    return summary

	
def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k


def add_user_hours_by_machineID(mach_id):
    rows = MainTable.query.filter_by(machine_id=mach_id).all()
    first_year = MainTable.query.filter_by(machine_id=mach_id).order_by(MainTable.date_time.asc()).first()
    last_year = MainTable.query.filter_by(machine_id=mach_id).order_by(MainTable.date_time.desc()).first()

    users_dic = {}
    total_users = UserTable.query.all()
    for usr in total_users:
        users_dic[usr.id] = usr.user_name

    if (first_year.date_time.year is None) or (last_year.date_time.year is None):
        return True

    year_range = range(int(first_year.date_time.year), int(last_year.date_time.year))  # TODO Make check NULL NoneType data

    for shit in year_range:
        for month in range(1,13):
            dat = datetime.strptime(str(shit)+'-'+str(month), "%Y-%m")
            days_in_month = calendar.monthrange(dat.year, dat.month)[1]

            nex = dat + timedelta(days=days_in_month)

            result = parse_log(rows,dat.strftime("%Y-%m-%d"),nex.strftime("%Y-%m-%d"))
            tmp = datetime.now()
            for r in result:
                #print(dat_in_sec,'====',result[r].days*24*3600,'====',result[r].total_seconds(), tmp.fromtimestamp(result[r].total_seconds()))
                row = UserHoursTable(user_id=get_key(users_dic,r),machine_id=mach_id,month=dat,hours=tmp.fromtimestamp(result[r].total_seconds()))
                db.session.add(row)
                db.session.commit()
    return True



def MakeLog():
    with open('config.yml') as f:
        obj_conf = yaml.load(f, Loader=yaml.FullLoader)
    machines = obj_conf.get('MACHINES')
    add_machine(machines)

    for machine in machines:
        print(machine, machines[machine])
        add_log(machine,machines[machine])




record = MachineTable.query.all()
for r in record:
    add_user_hours_by_machineID(r.id)
    print(r.id,"--- DONE")